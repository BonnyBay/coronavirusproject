#!/usr/bin/python3
# -*- coding: utf-8 -*-

import requests
import time
import datetime
import pytz
from pprint import pprint
from datetime import datetime, timedelta
import re, json
import pymysql
import traceback
from urllib.request import urlopen

import pandas as pd
import sys

import pandas as pd
import csv
import numpy as np
import math

giorniDaInserire = 30

#sjsjsjsj

db = pymysql.connect(host='BonnyBay.mysql.pythonanywhere-services.com', port=3306, user='BonnyBay', passwd='progettofisica', db='BonnyBay$coronavirusProject')
#db = pymysql.connect(host='localhost', port=3306, user='fscovid', passwd='progettofisica', db='coronavirusProject')

cursorCov = db.cursor()


def buildMartixOfDay(urlRegioni, line):
    try:
        df = pd.read_csv(urlRegioni, delimiter=',', error_bad_lines=False)
        df2 = pd.read_csv(urlRegioni, delimiter=',', error_bad_lines=False, chunksize=100000)
        i=0
        for row in df2:
            for i in range(len(df)):
                #qui riempio la matrice con i blocchi(righe) del csv
                line.append(row.iloc[i])
                i=i+1
        #riempio i campi nan di distretto e regione nella matrice line
        i=0
        print(len(df))
        while i<len(line): #lunghezza di line
            if str(line[i]['Province_State'])=='nan' or str(line[i]['Province_State']) == 'Unknown':
                line[i]['Province_State']='null'
            if str(line[i]['Recovered']) == 'nan':
                line[i]['Recovered']=0
            i = i + 1
    except:
        #print(traceback.format_exc())
        print('')

# ------------------- inserimento stati/regioni/distretti dentro la tabella POSIZIONE -------------------
def inserisciDatiStato(nomeStatoInput):
    try:
        # ------ NOTA: l'apice singolo" ' " per la valle d'Aosta genera problemi
        nomeStatoInput = nomeStatoInput.replace("'", '')


        cursorCov.execute("INSERT INTO STATO(nomeStato) \
                        VALUES ('%s')" % (nomeStatoInput))
        #print('nomeStato inserito')
        db.commit()
    except:
        #print(traceback.format_exc())
        print('')


def inserisciDatiRegione(nomeRegioneInput,nomeStatoInput):
    try:
        # ------ NOTA: l'apice singolo" ' " per la valle d'Aosta genera problemi
        nomeStatoInput = nomeStatoInput.replace("'", '')
        nomeRegioneInput = nomeRegioneInput.replace("'", '')


        cursorCov.execute("INSERT INTO REGIONE(nomeRegione,nomeStato) \
                        VALUES ('%s','%s')" % (nomeRegioneInput,nomeStatoInput))
        #print('nomeStato inserito')
        db.commit()
    except:
        #print(traceback.format_exc())
        print('')


dataDaCuiPartire = datetime.now().date() #- timedelta(days=51)
now = datetime.now()
if now.hour >= 8:
    yesterday = dataDaCuiPartire - timedelta(days=1)
    numeroGiorni = 1
else:
    yesterday = dataDaCuiPartire - timedelta(days=2)
    numeroGiorni = 2
yesterdaystr = yesterday.strftime("%m-%d-%Y")
urlRegioni = 'https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_daily_reports/{}.csv'.format(yesterdaystr)
#url che da problemi
#https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_daily_reports/11-11-2020.csv

line = []
buildMartixOfDay(urlRegioni,line)
i=0
print("Sto inserendo gli Stati e le Regioni...",len(line))
while i<len(line)-1:
    i = i+1
    inserisciDatiStato(line[i]['Country_Region'])
    if line[i]['Province_State'] != 'null':
        #print("REGIONE INSERITAAAAAAA")
        inserisciDatiRegione(line[i]['Province_State'],line[i]['Country_Region'])
    #inserisciDatiPosizione(line[i]['Country_Region'],line[i]['Province_State'],line[i]['Admin2'])



# ------------------------------------------------------------------------------------------------
# ------------------------------------------------------------------------------------------------
# ------------------- inserimento valori covid -------------------
def inserisciValoriCovidStato(nomeStatoInput, newPositivi, giornataDeiValoriCovid, newMorti, newGuariti, mortiTot, guaritiTot, positiviTot):
    try:

        cursorCov.execute("INSERT INTO VALORICOVIDSTATO(nomeStato, nuoviPositivi, dataValoriCovid, nuoviMorti, nuoviGuariti, mortiTotali, guaritiTotali, positiviTotali) \
                        VALUES ('%s','%s','%s','%s','%s','%s','%s','%s')" % (nomeStatoInput, newPositivi, giornataDeiValoriCovid, newMorti, newGuariti, mortiTot, guaritiTot, positiviTot))
        #print('valori covid inseriti')
        db.commit()
    except:
        #print(traceback.format_exc())
        print('')


def inserisciValoriCovidRegione(nomeStatoInput, nomeRegioneInput, newPositivi, giornataDeiValoriCovid, newMorti, newGuariti, mortiTot, guaritiTot, positiviTot):
    try:

        cursorCov.execute("INSERT INTO VALORICOVIDREGIONE(nomeStato, nomeRegione, nuoviPositivi, dataValoriCovid, nuoviMorti, nuoviGuariti, mortiTotali, guaritiTotali, positiviTotali) \
                        VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s')" % (nomeStatoInput, nomeRegioneInput, newPositivi, giornataDeiValoriCovid, newMorti, newGuariti, mortiTot, guaritiTot, positiviTot))
        #print('valori covid inseriti')
        db.commit()
    except:
        #print(traceback.format_exc())
        print('')

i = 0
numeroGiorni = 2

print("Adesso inserisco i valori covid per ogni giorno...")
while numeroGiorni<=giorniDaInserire+1:
    newPositivi = 0
    newMorti = 0
    newGuariti = 0
    positiviStato = 0
    mortiStato = 0
    guaritiStato = 0
    nomeStat = ""
    nomeReg = ""
    statiPassati = []

    print('------------------------ ALTRO GIROOOOOO ------------------------')
    line = []
    giornataDeiValoriCovid = dataDaCuiPartire - timedelta(numeroGiorni-1)
    strGiornataDeiValoriCovid = giornataDeiValoriCovid.strftime("%m-%d-%Y")
    urlCSVJH = 'https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_daily_reports/{}.csv'.format(strGiornataDeiValoriCovid)
    #print(urlCSVJH)
    buildMartixOfDay(urlCSVJH, line) #costruisco la matrice ogni giorno
    #print("Lunghezza csv di 11-11:",len(line))

    i = 1
    while i<=len(line)-1:

        try:
            positiviStato = 0
            mortiStato = 0
            guaritiStato = 0
            #print(len(line))
            nomeStat = line[i]['Country_Region']
            nomeStat = nomeStat.replace("'", '')
            cursorCov.execute("SELECT COUNT(nomeStato) FROM STATO WHERE nomeStato='"+nomeStat+"'")
            statoEsisteNelDB = cursorCov.fetchall()[0][0]
            #print(statoEsisteNelDB," nomestat: ",nomeStat, " i : ", i)
            if statoEsisteNelDB == 0:
                inserisciDatiStato(nomeStat)
            else:
                u = 1
                passato = False
                #print("stati:passati::::::::",statiPassati)
                for n in statiPassati:
                    #print("stati:enne::::::::",n)
                    #print("stati:enne::::::::",(n == nomeStat))
                    if n == nomeStat:
                        passato = True

                if not passato:
                    #print("###############################", not passato)
                    while u<=len(line)-1:
                        if line[u]['Country_Region'] == nomeStat:
                            #print("###############################statooooooooooooooooo", nomeStat)
                            positiviStato = positiviStato + line[u]['Confirmed']
                            mortiStato = mortiStato + line[u]['Deaths']
                            guaritiStato = guaritiStato + line[u]['Recovered']
                        u = u+1
                    statiPassati.append(nomeStat)
                    inserisciValoriCovidStato(nomeStat, newPositivi, giornataDeiValoriCovid, newMorti, newGuariti, mortiStato, guaritiStato, positiviStato )
                i = i+1
        except:
            #print(traceback.format_exc())
            print('')

    i = 1
    newPositivi = 0
    newMorti = 0
    newGuariti = 0
    positiviRegione = 0
    mortiRegione = 0
    guaritiRegione = 0
    nomeStat = ""
    nomeReg = ""
    regioniPassate = []
    while i<=len(line)-1:
        try:
            positiviRegione = 0
            mortiRegione = 0
            guaritiRegione = 0
            #print(len(line))
            nomeStat = line[i]['Country_Region']
            nomeStat = nomeStat.replace("'", '')
            nomeReg = line[i]['Province_State']
            nomeReg = nomeReg.replace("'", '')
            #print("iiiiiiii: "+str(i)+"    nomeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeReeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeegggggggggggggg           "+nomeReg)
            nomeRegStat = nomeReg+"-"+nomeStat

            if line[i]['Province_State'] != 'null':
                cursorCov.execute("SELECT COUNT(nomeRegione) FROM REGIONE WHERE nomeStato='"+nomeStat+"' AND nomeRegione='"+nomeReg+"'")
                regioneEsisteNelDB = cursorCov.fetchall()[0][0]
                #print(regioneEsisteNelDB," nomereg: ",nomeReg, " i : ", i)
                if regioneEsisteNelDB == 0:
                    inserisciDatiRegione(nomeReg,nomeStat)
                else:
                    u = 1
                    passato = False
                    #print("regioni:passati::::::::",regioniPassate)
                    for n in regioniPassate:
                        if n == nomeRegStat:
                            passato = True
                    if not passato:
                        #print("###############################", not passato)
                        while u<=len(line)-1:
                            if line[u]['Country_Region'] == nomeStat and line[u]['Province_State'] == nomeReg:
                                #print("###############################       "+line[u]['Province_State']+"     "+nomeRegStat +"     uuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuu"+line[u]['Country_Region'] )
                                positiviRegione = positiviRegione + line[u]['Confirmed']
                                mortiRegione = mortiRegione + line[u]['Deaths']
                                guaritiRegione = guaritiRegione + line[u]['Recovered']
                            u = u+1
                        regioniPassate.append(nomeRegStat)
                        inserisciValoriCovidRegione(nomeStat, nomeReg, newPositivi, giornataDeiValoriCovid, newMorti, newGuariti, mortiRegione, guaritiRegione, positiviRegione )
                    i = i+1
            else:
                i = i+1
        except:
            print(traceback.format_exc())
            print('')


    numeroGiorni = numeroGiorni + 1

# -------------------------- gestione nuovi positivi, morti, guariti --------------------------

if now.hour >= 8:
    numeroGiorni = 1
else:
    numeroGiorni = 2


while numeroGiorni<giorniDaInserire-1:
    giornataDeiValoriCovid = dataDaCuiPartire - timedelta(numeroGiorni)
    strGiornataDeiValoriCovid = giornataDeiValoriCovid.strftime("%Y-%m-%d")
    giornataDeiValoriCovidMenoUno = dataDaCuiPartire - timedelta(numeroGiorni+1)
    strGiornataDeiValoriCovidMenoUno = giornataDeiValoriCovidMenoUno.strftime("%Y-%m-%d")

    print(strGiornataDeiValoriCovid + "    "+ strGiornataDeiValoriCovidMenoUno)

    cursorCov.execute("SELECT nomeStato FROM STATO")
    stati = cursorCov.fetchall()
    cursorCov.execute("SELECT nomeRegione,nomeStato FROM REGIONE")
    regioni = cursorCov.fetchall()

    k=0
    while k<len(stati):

        cursorCov.execute("SELECT mortiTotali,guaritiTotali,positiviTotali FROM VALORICOVIDSTATO WHERE nomeStato='"+stati[k][0]+"' AND dataValoriCovid='"+strGiornataDeiValoriCovid+"'")
        statoGiornokPositivi = cursorCov.fetchall()

        #print(statoGiornokPositivi,"    ",statoGiornokMorti,"   ",statoGiornokGuariti)
        cursorCov.execute("SELECT mortiTotali,guaritiTotali,positiviTotali FROM VALORICOVIDSTATO WHERE nomeStato='"+stati[k][0]+"' AND dataValoriCovid='"+strGiornataDeiValoriCovidMenoUno+"'")
        statoGiornokPositiviMenoUno = cursorCov.fetchall()

        if len(statoGiornokPositivi) != 0 and len(statoGiornokPositiviMenoUno) != 0:
            #print(statoGiornokPositiviMenoUno,"    ",statoGiornokMortiMenoUno,"   ",statoGiornokGuaritiMenoUno)
            nuoviPositivi = int(statoGiornokPositivi[0][2])-int(statoGiornokPositiviMenoUno[0][2])
            nuoviMorti = int(statoGiornokPositivi[0][0])-int(statoGiornokPositiviMenoUno[0][0])
            nuoviGuariti = int(statoGiornokPositivi[0][1])-int(statoGiornokPositiviMenoUno[0][1])
            if(nuoviPositivi<0):
                nuoviPositivi = 0
            if(nuoviMorti<0):
                nuoviMorti = 0
            if(nuoviGuariti<0):
                nuoviGuariti = 0

            cursorCov.execute("UPDATE VALORICOVIDSTATO SET nuoviPositivi='"+str(nuoviPositivi)+"', nuoviMorti='"+str(nuoviMorti)+"', nuoviGuariti='"+str(nuoviGuariti)+"' WHERE nomeStato='"+stati[k][0]+"' AND dataValoriCovid='"+strGiornataDeiValoriCovid+"'")
            db.commit()
        k=k+1

    k=0
    while k<len(regioni):

        cursorCov.execute("SELECT mortiTotali, guaritiTotali, positiviTotali FROM VALORICOVIDREGIONE WHERE nomeregione='"+regioni[k][0]+"' AND nomeStato='"+regioni[k][1]+"' AND dataValoriCovid='"+strGiornataDeiValoriCovid+"'")
        statoGiornokPositivi = cursorCov.fetchall()

        cursorCov.execute("SELECT mortiTotali, guaritiTotali, positiviTotali FROM VALORICOVIDREGIONE WHERE nomeregione='"+regioni[k][0]+"' AND nomeStato='"+regioni[k][1]+"' AND dataValoriCovid='"+strGiornataDeiValoriCovidMenoUno+"'")
        statoGiornokPositiviMenoUno = cursorCov.fetchall()

        #print("k: ",k, "    ", statoGiornokPositivi,"   giorno: ", strGiornataDeiValoriCovid)
        #print("k: ",k, "    ", statoGiornokPositiviMenoUno,"   giorno: ", strGiornataDeiValoriCovidMenoUno)
        if len(statoGiornokPositivi) != 0 and len(statoGiornokPositiviMenoUno) != 0:
            nuoviPositivi = int(statoGiornokPositivi[0][2])-int(statoGiornokPositiviMenoUno[0][2])
            nuoviMorti = int(statoGiornokPositivi[0][0])-int(statoGiornokPositiviMenoUno[0][0])
            nuoviGuariti = int(statoGiornokPositivi[0][1])-int(statoGiornokPositiviMenoUno[0][1])
            if(nuoviPositivi<0):
                nuoviPositivi = 0
            if(nuoviMorti<0):
                nuoviMorti = 0
            if(nuoviGuariti<0):
                nuoviGuariti = 0

            cursorCov.execute("UPDATE VALORICOVIDREGIONE SET nuoviPositivi='"+str(nuoviPositivi)+"', nuoviMorti='"+str(nuoviMorti)+"', nuoviGuariti='"+str(nuoviGuariti)+"' WHERE nomeregione='"+regioni[k][0]+"' AND nomeStato='"+regioni[k][1]+"' AND dataValoriCovid='"+strGiornataDeiValoriCovid+"'")
            db.commit()
        k=k+1


    numeroGiorni = numeroGiorni+1

#rimozione delle ultime due righe dal db

try:
    giornataDeiValoriCovid = dataDaCuiPartire - timedelta(giorniDaInserire)
    ultimaData = giornataDeiValoriCovid.strftime("%Y-%m-%d")

    giornataDeiValoriCovidMenoUno = dataDaCuiPartire - timedelta(giorniDaInserire-1)
    penultimaData = giornataDeiValoriCovidMenoUno.strftime("%Y-%m-%d")


    cursorCov.execute("DELETE FROM VALORICOVIDSTATO WHERE dataValoriCovid = '"+str(ultimaData)+"' OR dataValoriCovid = '"+str(penultimaData)+"'")
    db.commit()
    cursorCov.execute("DELETE FROM VALORICOVIDREGIONE WHERE dataValoriCovid = '"+str(ultimaData)+"' OR dataValoriCovid = '"+str(penultimaData)+"'")
    db.commit()
    #cursorCov.execute("DELETE FROM VALORICOVID group by idDistretto order by id desc limit 2")

except:
    #print(traceback.format_exc())
    print('')



db.close()
