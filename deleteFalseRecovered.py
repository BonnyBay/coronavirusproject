import requests
import time
import datetime
import pytz
from pprint import pprint
from datetime import datetime, timedelta
import re, json
import pymysql
import traceback
from urllib.request import urlopen

import pandas as pd
import sys

import pandas as pd
import csv 
import numpy as np
import math
import matplotlib.pyplot as plt


#183.000 lo scarto

tempoGuarigionePositiviNuovi = 23 #tempo di guarigione dei nuovi positivi

db = pymysql.connect(host='localhost', port=3306, user='fscovid', passwd='progettofisica', db='coronavirusProject')
cursorCov = db.cursor()

#calcoliamo la somma dei nuovi positi degli ultimi 23 giorni, ovvero i non guariti, in questo caso diciamo che le persone che sono
#entrate in contatto con il virus sono o guariti o morti.
#formula da applicare: positiTotali - non guariti - mortiTotali(aggiunti da noi) = guaritiTotali(del csv) +- 20% dei guariti totali

cursorCov.execute("select nuovipositivi from VALORICOVIDSTATO where nomeStato='italy' order by datavaloricovid desc limit "+ str(tempoGuarigionePositiviNuovi) + "")
nuoviPosLastGG = cursorCov.fetchall()
nuoviPosTotGG = 0
for row in nuoviPosLastGG:
    nuoviPosTotGG = nuoviPosTotGG + row[0] #alla fine conterrà i positivi totali degli ultimi N giorni    

cursorCov.execute("select positiviTotali from VALORICOVIDSTATO where nomeStato='italy' order by datavaloricovid desc limit 1")
totPosLastGG = cursorCov.fetchall()[0][0]

#recupero i morti totali Italiani
cursorCov.execute("select mortiTotali from VALORICOVIDSTATO where nomeStato='italy' order by datavaloricovid desc limit 1")
totMortiLastGG = cursorCov.fetchall()[0][0]


approximateRecovered = totPosLastGG - nuoviPosTotGG - totMortiLastGG #nella migliore delle ipotesi devo ottenere i guariti totali del csv

cursorCov.execute("select guaritiTotali from VALORICOVIDSTATO where nomeStato='italy' order by datavaloricovid desc limit 1")
totRecLastGG = cursorCov.fetchall()[0][0]

#calcolo il range di approssimazione

percentuale = 17/100
ventiPerCentoDelCsv = totRecLastGG * percentuale

#print('Valore nuovi guariti calcolati dalla formula: ',approximateRecovered)
#print('Valore guariti totali del csv: ',totRecLastGG)
#print('DIFFERENZA TRA I DUE VALORI: ',approximateRecovered-totRecLastGG)

if((approximateRecovered > (totRecLastGG - ventiPerCentoDelCsv)) and (approximateRecovered < (totRecLastGG + ventiPerCentoDelCsv)) ):
    #print("---------------------------------------------------------------------------")
    #print("ci siamoooooo ERRATO")
    print("---------------------------------------------------------------------------")

else:
    print("non ci siamooooo")


#####################################################################
#####################################################################
################## DOPO L'INCONTRO CON IL PROF ######################
#####################################################################
#####################################################################

#CALCOLO I NUOVI GUARITI DEGLI ULTIMI 10 GIORNI (Sommo i guariti di ogni giorno degli ultimi 10 giorni)
altriGiorni = 10

cursorCov.execute("select guaritiTotali from VALORICOVIDSTATO where nomeStato='italy' order by datavaloricovid desc limit 1")
guariti_totali_ultimo_giorno = cursorCov.fetchall()[0][0]
#print(guariti_totali_ultimo_giorno)
cursorCov.execute("select guaritiTotali from VALORICOVIDSTATO where nomeStato='italy' order by datavaloricovid desc limit "+str(altriGiorni)+"")
guariti_totali_dieci_giorni_fa = cursorCov.fetchall()[altriGiorni-1][0]  #prendo i guariti di N giorni fa (N è altriGiorni-1 perchè parto da 0)

#print(guariti_totali_dieci_giorni_fa)

somma_guariti_degli_utlimiDieciGiorni = guariti_totali_ultimo_giorno - guariti_totali_dieci_giorni_fa



#CALCOLO GLI INFETTI DEGLI ULTIMI 25 GIORNI
NgiorniConsiderati = 25

cursorCov.execute("select nuovipositivi from VALORICOVIDSTATO where nomeStato='italy' order by datavaloricovid desc limit "+ str(NgiorniConsiderati) + "")
nuoviPositiviRisultatoQuery = cursorCov.fetchall()
sommaPositiviUltimiNGiorni = 0
for row in nuoviPositiviRisultatoQuery:
    sommaPositiviUltimiNGiorni = sommaPositiviUltimiNGiorni + row[0] 


#CALCOLO I GUARITI DEI 10 GIORNI PRIMA I 25 GIORNI DEI NUOVI POSITI


NgiorniConsideratiPiuDieci = NgiorniConsiderati + altriGiorni
cursorCov.execute("select nuovipositivi from VALORICOVIDSTATO where nomeStato='italy' order by datavaloricovid desc limit "+ str(NgiorniConsideratiPiuDieci) + "")
nuoviPositiviRisultatoQuery = cursorCov.fetchall()
sommaGuaritiPassati = 0
count = 0
for row in nuoviPositiviRisultatoQuery:
    if(count>25): #sommo quelli dalla giorna di 25 giorni fa a 35 giorni fa 
        sommaGuaritiPassati = sommaGuaritiPassati + row[0] 

    count = count + 1 


#verifica dell'uguaglianza
print("Guariti degli ultimi 10 giorni calcolati dalla colonan guariti: ",somma_guariti_degli_utlimiDieciGiorni)
print("Dovrebbe essere uguale a:")
print("Guariti degli ultimi 10 giorni (25 giorni fa meno altri 10 giorni = max 35 giorni fa)calcolati dalla colonna nuovi positivi: ",sommaGuaritiPassati)



###########################################################################################
###########################################################################################
############################### CALCOLO DELLA MEDIA MOBILE ################################
###########################################################################################
###########################################################################################


#devo prendere i valori da 4 giorni fa ai primi valori inseriti + 4 per star sicuro e star dentro alla media

#la media mobile si calcola: media dei valori precedenti piu 3 giorni successivi ad oggi piu valore di oggi
################################################################################################
################################ INIZIAMO ######################################################
################################################################################################

#recupero 3 valori precedenti ad oggi, MA PRIMA mi posiziono a 4 giorni fa e recupero il valore dei nuovi positivi
print("\n\t\t\t\t------------------------------------------------------------")
print("\t\t\t\t-----------------Calcolo media mobile-----------------------")
print("\t\t\t\t------------------------------------------------------------\n")

print("\t\t\t\t------------------- Uso i nuovi positivi -------------------\n")

#############
###########
########### IL MECCANISMO SOTTOSTANTE è DA RIPETER PER TUTTI I GIORNI NEL DATABASE
##########

cursorCov.execute("select nuovipositivi from VALORICOVIDSTATO where nomeStato='italy' order by datavaloricovid desc limit "+ str(11) + "") #7+4 = 11
valoreDelQuartoGiorno = cursorCov.fetchall()[3][0] #valori del 4 giorno
valoriTreGiorniPrecedentiAlQuartoGiorno = [] #è un vettore
valoriTreGiorniSuccessiviAlQuartoGiorno = []
conteggio = 0 #per contare giorni prima e giorni dopo al 4 giorno

cursorCov.execute("select nuovipositivi from VALORICOVIDSTATO where nomeStato='italy' order by datavaloricovid desc limit "+ str(11) + "") #7+4 = 11
valoriTotFetchatiBene = cursorCov.fetchall()
for riga in valoriTotFetchatiBene:
    if(conteggio<3):
        valoriTreGiorniPrecedentiAlQuartoGiorno.append(riga[0])
    elif (conteggio>=4 and conteggio < 7):
        valoriTreGiorniSuccessiviAlQuartoGiorno.append(riga[0])
    conteggio = conteggio + 1

print("valori successivi: ", valoriTreGiorniPrecedentiAlQuartoGiorno)
print("valore centrale: ", valoreDelQuartoGiorno)
print("valori precedenti ", valoriTreGiorniSuccessiviAlQuartoGiorno)



print("\n\t\t\t\t------------------------------------------------------------")
print("\t\t\t\t-----------------Calcolo media mobile PER TUTTI I GIORNI NEL DB-----------------------")
print("\t\t\t\t------------------------------------------------------------\n")




############## procedimento di prima ripetuto per tutte le giornate del dp

vetI = [] #serve solo per graficare su python
vettore_MediaMobile_NuoviPositivi = [] #inizializzo il vettore
cursorCov.execute("select count(*) from VALORICOVIDSTATO where nomeStato='italy' order by datavaloricovid ") 
contaValNelDB = cursorCov.fetchall()
#print("numero righe estratte dal DB: ", contaValNelDB)
cursorCov.execute("select nuovipositivi, datavaloricovid from VALORICOVIDSTATO where nomeStato='italy' order by datavaloricovid ") 
tuttiPositiviNelDB = cursorCov.fetchall()
threeValueBefore = []
threeValueAfter = []
valueCentral = 0
threeValueBeforeData = []
threeValueAfterData = []
valueCentralData = 0


cunterGiornateMedia = 0
somma = 0
media = 0
#for cella in tuttiPositiviNelDB:
i = 0 #scorro il contenuto del db ottenuto dalla query
while i < len(tuttiPositiviNelDB):
    vetI.append(i)
    cursorCov.execute("select nuovipositivi,datavaloricovid from VALORICOVIDSTATO where nomeStato='italy' order by datavaloricovid ") 
    pony = cursorCov.fetchall()
    #rint(i)
    #print(cunterGiornateMedia)
    #print(i)

    if(cunterGiornateMedia<3):
        #threeValueBefore.append(cella[0])
        threeValueBefore.append(pony[i][0])
        threeValueBeforeData.append(pony[i][1])

    elif(cunterGiornateMedia==3): #sono al val centrale, ovvero il quarto valore
        #valueCentral = cella[0]
        valueCentral = pony[i][0]
        valueCentralData = pony[i][1]


    elif (cunterGiornateMedia>3):
        #threeValueAfter.append(cella[0])
        threeValueAfter.append(pony[i][0])
        threeValueAfterData.append(pony[i][1])



    #calcolo la media in base ai valori salvati e puoi svuoto tutto
    #print(threeValueAfter)

    if(cunterGiornateMedia == 6):
        print("3 day before ",threeValueBefore)
        print("data before: ",threeValueBeforeData)
        print("central value ",valueCentral)
        print("central value data: ",valueCentralData)

        print("3 day after ",threeValueAfter)
        print("3 day after Data: ",threeValueAfterData)

        print("\n")
        somma = threeValueBefore[0] + threeValueBefore[1] + threeValueBefore[2] + valueCentral + threeValueAfter[0] + threeValueAfter[1] + threeValueAfter[2]
        media = somma / 7
        vettore_MediaMobile_NuoviPositivi.append(round(media,2))
        print("----- MEDIA CALCOLATA: ",media)
        #print("media aggiunta ", media)

        #ri-azzero tutto
        threeValueBefore = []
        threeValueAfter = []
        valueCentral = 0
        threeValueBeforeData = []
        threeValueAfterData = []
        valueCentralData = 0
        somma = 0
        media = 0
        #cunterGiornateMedia = 0
        

    #IDENTATO QUIIIII
    cunterGiornateMedia = cunterGiornateMedia + 1
    i = i + 1

    if(cunterGiornateMedia == 7):#azzero il valore per ricominciare con una nuova media 
        cunterGiornateMedia = 0
        i = i-6

#print("media mobile vera calolataaaaaa")
#print(vettore_MediaMobile_NuoviPositivi)


#grafico
plt.plot(vettore_MediaMobile_NuoviPositivi)
plt.ylabel(vetI)
#plt.show()



cursorCov.execute("select nuovipositivi from VALORICOVIDSTATO where nomeStato='italy' order by datavaloricovid ") 
positiviNelDatabase = cursorCov.fetchall()

plt.plot(positiviNelDatabase)
plt.ylabel(vetI)
plt.show()




print("\t\t\t\t------------------- Uso i nuovi guariti -------------------\n")

#############
###########
########### IL MECCANISMO SOTTOSTANTE è DA RIPETER PER TUTTI I GIORNI NEL DATABASE
##########

cursorCov.execute("select nuoviGuariti from VALORICOVIDSTATO where nomeStato='italy' order by datavaloricovid desc limit "+ str(11) + "") #7+4 = 11
valoreDelQuartoGiorno = cursorCov.fetchall()[3][0] #valori del 4 giorno
valoriTreGiorniPrecedentiAlQuartoGiorno = [] #è un vettore
valoriTreGiorniSuccessiviAlQuartoGiorno = []
conteggio = 0 #per contare giorni prima e giorni dopo al 4 giorno

cursorCov.execute("select nuoviGuariti from VALORICOVIDSTATO where nomeStato='italy' order by datavaloricovid desc limit "+ str(11) + "") #7+4 = 11
valoriTotFetchatiBene = cursorCov.fetchall()
for riga in valoriTotFetchatiBene:
    if(conteggio<3):
        valoriTreGiorniPrecedentiAlQuartoGiorno.append(riga[0])
    elif (conteggio>=4 and conteggio < 7):
        valoriTreGiorniSuccessiviAlQuartoGiorno.append(riga[0])
    conteggio = conteggio + 1



print("\n\t\t\t\t------------------------------------------------------------")
print("\t\t\t\t-----------------Calcolo media mobile PER TUTTI I GIORNI NEL DB per i guariti-----------------------")
print("\t\t\t\t------------------------------------------------------------\n")




############## procedimento di prima ripetuto per tutte le giornate del dp

vetI = [] #serve solo per graficare su python
vettore_MediaMobile_NuoviPositivi = [] #inizializzo il vettore
cursorCov.execute("select count(*) from VALORICOVIDSTATO where nomeStato='italy' order by datavaloricovid ") 
contaValNelDB = cursorCov.fetchall()
print("numero righe estratte dal DB: ", contaValNelDB)
cursorCov.execute("select nuoviGuariti from VALORICOVIDSTATO where nomeStato='italy' order by datavaloricovid ") 
tuttiPositiviNelDB = cursorCov.fetchall()
threeValueBefore = []
threeValueAfter = []
valueCentral = 0
cunterGiornateMedia = 0
somma = 0
media = 0
#for cella in tuttiPositiviNelDB:
i = 0 #scorro il contenuto del db ottenuto dalla query
while i < len(tuttiPositiviNelDB):
    vetI.append(i)
    cursorCov.execute("select nuoviGuariti from VALORICOVIDSTATO where nomeStato='italy' order by datavaloricovid ") 
    print(i)
    #print(cunterGiornateMedia)
    #print(i)

    if(cunterGiornateMedia<3):
        #threeValueBefore.append(cella[0])
        threeValueBefore.append(cursorCov.fetchall()[i][0])
    elif(cunterGiornateMedia==3): #sono al val centrale, ovvero il quarto valore
        #valueCentral = cella[0]
        valueCentral = cursorCov.fetchall()[i][0]

    elif (cunterGiornateMedia>4):
        #threeValueAfter.append(cella[0])
        threeValueAfter.append(cursorCov.fetchall()[i][0])


    #calcolo la media in base ai valori salvati e puoi svuoto tutto
    #print(threeValueAfter)

    if(cunterGiornateMedia == 7):
        somma = threeValueBefore[0] + threeValueBefore[1] + threeValueBefore[2] + valueCentral + threeValueAfter[0] + threeValueAfter[1] + threeValueAfter[2]
        media = somma / 7
        vettore_MediaMobile_NuoviPositivi.append(round(media,2))
        #print("media aggiunta ", media)

        #ri-azzero tutto
        threeValueBefore = []
        threeValueAfter = []
        valueCentral = 0
        somma = 0
        media = 0
        #cunterGiornateMedia = 0
        

    #IDENTATO QUIIIII
    cunterGiornateMedia = cunterGiornateMedia + 1
    i = i + 1

    if(cunterGiornateMedia == 8):#azzero il valore per ricominciare con una nuova media 
        cunterGiornateMedia = 0
        i = i-7



#grafico
plt.plot(vettore_MediaMobile_NuoviPositivi)
plt.ylabel(vetI)
#plt.show()



cursorCov.execute("select nuoviGuariti from VALORICOVIDSTATO where nomeStato='italy' order by datavaloricovid ") 
positiviNelDatabase = cursorCov.fetchall()

plt.plot(positiviNelDatabase)
plt.ylabel(vetI)
plt.show()



db.close()


