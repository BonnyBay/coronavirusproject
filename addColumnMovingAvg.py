import requests
import time
import datetime
import pytz
from pprint import pprint
from datetime import datetime, timedelta
import re, json
import pymysql
import traceback
from urllib.request import urlopen

import pandas as pd
import sys

import pandas as pd
import csv
import numpy as np
import math


db = pymysql.connect(host='BonnyBay.mysql.pythonanywhere-services.com', port=3306, user='BonnyBay', passwd='progettofisica', db='BonnyBay$coronavirusProject')
#db = pymysql.connect(host='127.0.0.1', port=3306, user='fscovid', passwd='progettofisica', db='coronavirusProject')
#db = pymysql.connect(host='localhost', port=3306, user='fscovid', passwd='progettofisica', db='coronavirusProject')

cursorCov = db.cursor()
try:
    cursorCov.execute("ALTER TABLE VALORICOVIDSTATO ADD COLUMN movingAvgPos VARCHAR(50);")
    db.commit()
    cursorCov.execute("ALTER TABLE VALORICOVIDREGIONE ADD COLUMN movingAvgPos VARCHAR(50);")
    db.commit()
    cursorCov.execute("ALTER TABLE VALORICOVIDSTATO ADD COLUMN movingAvgGua VARCHAR(50);")
    db.commit()
    cursorCov.execute("ALTER TABLE VALORICOVIDREGIONE ADD COLUMN movingAvgGua VARCHAR(50);")
    db.commit()
except:
    #print(traceback.format_exc())
    print('')

def calcoloMedieMobiliNuoviPositiviStato(nome):
    #-------------------------------------------------------------------------------
    # --------------- Inizio calcolo media mobile per i nuovi positivi ---------------
    #-------------------------------------------------------------------------------


    # PER PRIMA COSA DEVO CALCOLARE LA MEDIA MOBILE DEI NUOVI POSITIVI E DI GUARITI
    vettore_MediaMobile_NuoviPositivi = [] #inizializzo il vettore
    cursorCov.execute("select count(*) from VALORICOVIDSTATO where nomeStato='"+str(nome)+"' AND dataValoriCovid > '2021-02-12' order by dataValoriCovid ")
    contaValNelDB = cursorCov.fetchall()
    #print("numero righe estratte dal DB: ", contaValNelDB)
    cursorCov.execute("select nuovipositivi from VALORICOVIDSTATO where nomeStato='"+str(nome)+"' AND dataValoriCovid > '2021-02-12' order by dataValoriCovid ")
    tuttiPositiviNelDB = cursorCov.fetchall()
    threeValueBefore = []
    threeValueAfter = []
    valueCentral = 0
    cunterGiornateMedia = 0
    somma = 0
    media = 0
    #for cella in tuttiPositiviNelDB:
    i = 0 #scorro il contenuto del db ottenuto dalla query
    while i < len(tuttiPositiviNelDB):
        cursorCov.execute("select nuovipositivi from VALORICOVIDSTATO where nomeStato='"+str(nome)+"' AND dataValoriCovid > '2021-02-12' order by dataValoriCovid ")
        #print(cunterGiornateMedia)
        #print(i)

        if(cunterGiornateMedia<3):
            #threeValueBefore.append(cella[0])
            threeValueBefore.append(cursorCov.fetchall()[i][0])
        elif(cunterGiornateMedia==3): #sono al val centrale, ovvero il quarto valore
            #valueCentral = cella[0]
            valueCentral = cursorCov.fetchall()[i][0]

        elif (cunterGiornateMedia>3):
            #threeValueAfter.append(cella[0])
            threeValueAfter.append(cursorCov.fetchall()[i][0])


        #calcolo la media in base ai valori salvati e puoi svuoto tutto
        #print(threeValueAfter)

        if(cunterGiornateMedia == 6):

            somma = threeValueBefore[0] + threeValueBefore[1] + threeValueBefore[2] + valueCentral + threeValueAfter[0] + threeValueAfter[1] + threeValueAfter[2]
            media = somma / 7
            vettore_MediaMobile_NuoviPositivi.append(round(media,2))
            #print("media aggiunta ", media)

            #ri-azzero tutto
            threeValueBefore = []
            threeValueAfter = []
            valueCentral = 0
            somma = 0
            media = 0
            #cunterGiornateMedia = 0


        #IDENTATO QUIIIII
        cunterGiornateMedia = cunterGiornateMedia + 1
        i = i + 1

        if(cunterGiornateMedia == 7):#azzero il valore per ricominciare con una nuova media
            cunterGiornateMedia = 0
            i = i-6

    #print("media mobile vera calolataaaaaa")

    return vettore_MediaMobile_NuoviPositivi

    #-------------------------------------------------------------------------------
    # --------------- fine calcolo media mobile per i nuovi positivi ---------------
    #-------------------------------------------------------------------------------



def calcoloMedieMobiliNuoviGuaritiStato(nome):
    #-------------------------------------------------------------------------------
    # --------------- Inizio calcolo media mobile per i nuovi guariti ---------------
    #-------------------------------------------------------------------------------


    # PER PRIMA COSA DEVO CALCOLARE LA MEDIA MOBILE DEI NUOVI POSITIVI E DI GUARITI
    vettore_MediaMobile_NuoviGuariti = [] #inizializzo il vettore
    cursorCov.execute("SELECT count(*) from VALORICOVIDSTATO where nomeStato='"+str(nome)+"' AND dataValoriCovid > '2021-02-12' order by dataValoriCovid ")
    contaValNelDB = cursorCov.fetchall()

    cursorCov.execute("select nuoviGuariti from VALORICOVIDSTATO where nomeStato='"+str(nome)+"' AND dataValoriCovid > '2021-02-12' order by dataValoriCovid ")
    tuttiPositiviNelDB = cursorCov.fetchall()
    threeValueBefore = []
    threeValueAfter = []
    valueCentral = 0
    cunterGiornateMedia = 0
    somma = 0
    media = 0
    #for cella in tuttiPositiviNelDB:
    i = 0 #scorro il contenuto del db ottenuto dalla query
    while i < len(tuttiPositiviNelDB):
        cursorCov.execute("select nuoviGuariti from VALORICOVIDSTATO where nomeStato='"+str(nome)+"' AND dataValoriCovid > '2021-02-12' order by dataValoriCovid ")

        #print(cunterGiornateMedia)
        #print(i)

        if(cunterGiornateMedia<3):
            #threeValueBefore.append(cella[0])
            threeValueBefore.append(cursorCov.fetchall()[i][0])
        elif(cunterGiornateMedia==3): #sono al val centrale, ovvero il quarto valore
            #valueCentral = cella[0]
            valueCentral = cursorCov.fetchall()[i][0]

        elif (cunterGiornateMedia>3):
            #threeValueAfter.append(cella[0])
            threeValueAfter.append(cursorCov.fetchall()[i][0])


        #calcolo la media in base ai valori salvati e puoi svuoto tutto
        #print(threeValueAfter)

        if(cunterGiornateMedia == 6):

            somma = threeValueBefore[0] + threeValueBefore[1] + threeValueBefore[2] + valueCentral + threeValueAfter[0] + threeValueAfter[1] + threeValueAfter[2]
            media = somma / 7
            vettore_MediaMobile_NuoviGuariti.append(round(media,2))
            #print("media aggiunta ", media)

            #ri-azzero tutto
            threeValueBefore = []
            threeValueAfter = []
            valueCentral = 0
            somma = 0
            media = 0
            #cunterGiornateMedia = 0


        #IDENTATO QUIIIII
        cunterGiornateMedia = cunterGiornateMedia + 1
        i = i + 1

        if(cunterGiornateMedia == 7):#azzero il valore per ricominciare con una nuova media
            cunterGiornateMedia = 0
            i = i-6

    #print("media mobile vera calolata per lo stato: (per visionare i valori di tale media, scommentare la riga print vettore_MediaMobile_NuoviGuariti", nome)
    #print(vettore_MediaMobile_NuoviGuariti)

    return vettore_MediaMobile_NuoviGuariti
    #-------------------------------------------------------------------------------
    # --------------- fine calcolo media mobile per i nuovi guariti ---------------
    #-------------------------------------------------------------------------------

#########################################################################################
#########################################################################################
############################## media mobile per le regioni ##############################
#########################################################################################
#########################################################################################

def calcoloMedieMobiliNuoviPositiviRegione(stato,nome):
    #-------------------------------------------------------------------------------
    # --------------- Inizio calcolo media mobile per i nuovi positivi ---------------
    #-------------------------------------------------------------------------------


    # PER PRIMA COSA DEVO CALCOLARE LA MEDIA MOBILE DEI NUOVI POSITIVI E DI GUARITI
    vettore_MediaMobile_NuoviPositivi = [] #inizializzo il vettore
    cursorCov.execute("select count(*) from VALORICOVIDREGIONE where nomeStato='"+str(stato)+"' and nomeRegione='"+str(nome)+"' AND dataValoriCovid > '2021-02-12' order by dataValoriCovid ")
    contaValNelDB = cursorCov.fetchall()
    #print("numero righe estratte dal DB: ", contaValNelDB)
    cursorCov.execute("select nuovipositivi from VALORICOVIDREGIONE where nomeStato='"+str(stato)+"' and nomeRegione='"+str(nome)+"' AND dataValoriCovid > '2021-02-12' order by dataValoriCovid ")
    tuttiPositiviNelDB = cursorCov.fetchall()
    threeValueBefore = []
    threeValueAfter = []
    valueCentral = 0
    cunterGiornateMedia = 0
    somma = 0
    media = 0
    #for cella in tuttiPositiviNelDB:
    i = 0 #scorro il contenuto del db ottenuto dalla query
    while i < len(tuttiPositiviNelDB):
        cursorCov.execute("select nuovipositivi from VALORICOVIDREGIONE where nomeStato='"+str(stato)+"' and nomeRegione='"+str(nome)+"' AND dataValoriCovid > '2021-02-12' order by dataValoriCovid ")
        #print(cunterGiornateMedia)
        #print(i)

        if(cunterGiornateMedia<3):
            #threeValueBefore.append(cella[0])
            threeValueBefore.append(cursorCov.fetchall()[i][0])
        elif(cunterGiornateMedia==3): #sono al val centrale, ovvero il quarto valore
            #valueCentral = cella[0]
            valueCentral = cursorCov.fetchall()[i][0]

        elif (cunterGiornateMedia>3):
            #threeValueAfter.append(cella[0])
            threeValueAfter.append(cursorCov.fetchall()[i][0])


        #calcolo la media in base ai valori salvati e puoi svuoto tutto
        #print(threeValueAfter)

        if(cunterGiornateMedia == 6):

            somma = threeValueBefore[0] + threeValueBefore[1] + threeValueBefore[2] + valueCentral + threeValueAfter[0] + threeValueAfter[1] + threeValueAfter[2]
            media = somma / 7
            vettore_MediaMobile_NuoviPositivi.append(round(media,2))
            #print("media aggiunta ", media)

            #ri-azzero tutto
            threeValueBefore = []
            threeValueAfter = []
            valueCentral = 0
            somma = 0
            media = 0
            #cunterGiornateMedia = 0


        #IDENTATO QUIIIII
        cunterGiornateMedia = cunterGiornateMedia + 1
        i = i + 1

        if(cunterGiornateMedia == 7):#azzero il valore per ricominciare con una nuova media
            cunterGiornateMedia = 0
            i = i-6

    #print("media mobile vera calolataaaaaa")

    return vettore_MediaMobile_NuoviPositivi

    #-------------------------------------------------------------------------------
    # --------------- fine calcolo media mobile per i nuovi positivi ---------------
    #-------------------------------------------------------------------------------



def calcoloMedieMobiliNuoviGuaritiRegione(stato,nome):
    #-------------------------------------------------------------------------------
    # --------------- Inizio calcolo media mobile per i nuovi guariti ---------------
    #-------------------------------------------------------------------------------


    # PER PRIMA COSA DEVO CALCOLARE LA MEDIA MOBILE DEI NUOVI POSITIVI E DI GUARITI
    vettore_MediaMobile_NuoviGuariti = [] #inizializzo il vettore
    cursorCov.execute("select count(*) from VALORICOVIDREGIONE where nomeStato='"+str(stato)+"' and nomeRegione='"+str(nome)+"' AND dataValoriCovid > '2021-02-12' order by dataValoriCovid ")
    contaValNelDB = cursorCov.fetchall()

    cursorCov.execute("select nuoviGuariti from VALORICOVIDREGIONE where nomeStato='"+str(stato)+"' and nomeRegione='"+str(nome)+"' AND dataValoriCovid > '2021-02-12' order by dataValoriCovid ")
    tuttiPositiviNelDB = cursorCov.fetchall()
    threeValueBefore = []
    threeValueAfter = []
    valueCentral = 0
    cunterGiornateMedia = 0
    somma = 0
    media = 0
    #for cella in tuttiPositiviNelDB:
    i = 0 #scorro il contenuto del db ottenuto dalla query
    while i < len(tuttiPositiviNelDB):
        cursorCov.execute("select nuoviGuariti from VALORICOVIDREGIONE where nomeStato='"+str(stato)+"' and nomeRegione='"+str(nome)+"' AND dataValoriCovid > '2021-02-12' order by dataValoriCovid ")

        #print(cunterGiornateMedia)
        #print(i)

        if(cunterGiornateMedia<3):
            #threeValueBefore.append(cella[0])
            threeValueBefore.append(cursorCov.fetchall()[i][0])
        elif(cunterGiornateMedia==3): #sono al val centrale, ovvero il quarto valore
            #valueCentral = cella[0]
            valueCentral = cursorCov.fetchall()[i][0]

        elif (cunterGiornateMedia>3):
            #threeValueAfter.append(cella[0])
            threeValueAfter.append(cursorCov.fetchall()[i][0])


        #calcolo la media in base ai valori salvati e puoi svuoto tutto
        #print(threeValueAfter)

        if(cunterGiornateMedia == 6):

            somma = threeValueBefore[0] + threeValueBefore[1] + threeValueBefore[2] + valueCentral + threeValueAfter[0] + threeValueAfter[1] + threeValueAfter[2]
            media = somma / 7
            vettore_MediaMobile_NuoviGuariti.append(round(media,2))
            #print("media aggiunta ", media)

            #ri-azzero tutto
            threeValueBefore = []
            threeValueAfter = []
            valueCentral = 0
            somma = 0
            media = 0
            #cunterGiornateMedia = 0


        #IDENTATO QUIIIII
        cunterGiornateMedia = cunterGiornateMedia + 1
        i = i + 1

        if(cunterGiornateMedia == 7):#azzero il valore per ricominciare con una nuova media
            cunterGiornateMedia = 0
            i = i-6

    #print("media mobile vera calolata per la regione: (per visionare i valori di tale media, scommentare la riga print vettore_MediaMobile_NuoviGuariti", nome)
    #print(vettore_MediaMobile_NuoviGuariti)

    return vettore_MediaMobile_NuoviGuariti
    #-------------------------------------------------------------------------------
    # --------------- fine calcolo media mobile per i nuovi guariti ---------------
    #-------------------------------------------------------------------------------

#array stati
cursorCov.execute("SELECT nomeStato FROM STATO")
stati = cursorCov.fetchall()
#array regioni
cursorCov.execute("SELECT nomeRegione FROM REGIONE")
regioni = cursorCov.fetchall()

cursorCov.execute("SELECT dataValoriCovid FROM VALORICOVIDSTATO WHERE nomeStato ='Italy' AND dataValoriCovid > '2021-02-12' order by dataValoriCovid")
date = cursorCov.fetchall()
date = list(date)

#calcolo media mobile positivi e guariti regione per regione
for stato in stati:
    stato = str(stato).replace(',', '')
    stato = str(stato).replace(')', '')
    stato = str(stato).replace('(', '')
    stato = str(stato).replace("'", "")
    for regione in regioni:
        regione = str(regione).replace(',', '')
        regione = str(regione).replace(')', '')
        regione = str(regione).replace('(', '')
        regione = str(regione).replace("'", "")
        MedieMobiliNuoviGuaritiRegione = calcoloMedieMobiliNuoviGuaritiRegione(stato,regione)
        MedieMobiliNuoviPositiviRegione = calcoloMedieMobiliNuoviPositiviRegione(stato,regione)
        i=3
        #print(regione)
        for el in MedieMobiliNuoviGuaritiRegione:
            date[i] = str(date[i]).replace(',', '')
            date[i] = str(date[i]).replace(')', '')
            date[i] = str(date[i]).replace('(', '')
            date[i] = str(date[i]).replace("'", "")
            cursorCov.execute("UPDATE VALORICOVIDREGIONE SET movingAvgGua = '"+str(el)+"' WHERE nomeStato = '"+str(stato)+"' AND nomeRegione = '"+str(regione)+"' AND dataValoriCovid = '"+str(date[i])+"'")
            db.commit()
            i=i+1
        i=3
        for el in MedieMobiliNuoviPositiviRegione:
            date[i] = str(date[i]).replace(',', '')
            date[i] = str(date[i]).replace(')', '')
            date[i] = str(date[i]).replace('(', '')
            date[i] = str(date[i]).replace("'", "")
            cursorCov.execute("UPDATE VALORICOVIDREGIONE SET movingAvgPos = '"+str(el)+"' WHERE nomeStato = '"+str(stato)+"' AND nomeRegione = '"+str(regione)+"' AND dataValoriCovid = '"+str(date[i])+"'")
            db.commit()
            i=i+1
#calcolo media mobile positivi e guariti stato per stato
for stato in stati:
    stato = str(stato).replace(',', '')
    stato = str(stato).replace(')', '')
    stato = str(stato).replace('(', '')
    stato = str(stato).replace("'", "")
    MedieMobiliNuoviGuaritiStato = calcoloMedieMobiliNuoviGuaritiStato(stato)
    MedieMobiliNuoviPositiviStato = calcoloMedieMobiliNuoviPositiviStato(stato)
    i=3
    for el in MedieMobiliNuoviGuaritiStato:
        date[i] = str(date[i]).replace(',', '')
        date[i] = str(date[i]).replace(')', '')
        date[i] = str(date[i]).replace('(', '')
        date[i] = str(date[i]).replace("'", "")
        cursorCov.execute("UPDATE VALORICOVIDSTATO SET movingAvgGua = '"+str(el)+"' WHERE nomeStato = '"+str(stato)+"' AND dataValoriCovid = '"+str(date[i])+"'")
        db.commit()
        i=i+1
    i=3
    for el in MedieMobiliNuoviPositiviStato:
        date[i] = str(date[i]).replace(',', '')
        date[i] = str(date[i]).replace(')', '')
        date[i] = str(date[i]).replace('(', '')
        date[i] = str(date[i]).replace("'", "")
        cursorCov.execute("UPDATE VALORICOVIDSTATO SET movingAvgPos = '"+str(el)+"' WHERE nomeStato = '"+str(stato)+"' AND dataValoriCovid = '"+str(date[i])+"'")
        db.commit()
        i=i+1

db.close()




