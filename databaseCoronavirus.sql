drop DATABASE if exists BonnyBay$coronavirusProject;
CREATE DATABASE BonnyBay$coronavirusProject;
USE BonnyBay$coronavirusProject;


/*
PER RUNNARE IN LOCALE USARE:

drop DATABASE if exists coronavirusProject;
CREATE DATABASE coronavirusProject;
USE coronavirusProject;*/

/*
CREATE TABLE STATO (
  nome varchar (80) primary key,
  dataInserimento timeStamp default current_timestamp
) ENGINE=InnoDB;



CREATE TABLE REGIONE (

  #id int auto_increment primary key,
  nomeStato varchar (80) references STATO(nome),
  nome varchar (80),
  PRIMARY KEY(nomeStato,nome)

) ENGINE=InnoDB; */


CREATE TABLE STATO (
  nomeStato varchar (80) primary key

) ENGINE=InnoDB;

CREATE TABLE REGIONE (
  nomeRegione varchar (80),
  nomeStato varchar (80) references STATO(nomeStato),
  PRIMARY KEY(nomeRegione,nomeStato)

) ENGINE=InnoDB;

CREATE TABLE VALORICOVIDSTATO (

  nomeStato varchar (80) references STATO(nomeStato),
  nuoviPositivi int default 0,
  dataValoriCovid varchar (50),
  nuoviMorti int default 0,
  nuoviGuariti int default 0,
  mortiTotali bigint default 0,
  guaritiTotali bigint default 0,
  positiviTotali bigint default 0,
  primary key (nomeStato, dataValoriCovid)

) ENGINE=InnoDB;

CREATE TABLE VALORICOVIDREGIONE (
  nomeStato varchar (80) references REGIONE(nomeStato),
  nomeRegione varchar (80) references REGIONE(nomeRegione),
  nuoviPositivi int default 0,
  dataValoriCovid varchar (50),
  nuoviMorti int default 0,
  nuoviGuariti int default 0,
  mortiTotali bigint default 0,
  guaritiTotali bigint default 0,
  positiviTotali bigint default 0,
  primary key (nomeStato, nomeRegione, dataValoriCovid)

) ENGINE=InnoDB;

