from django.http import HttpResponse
from django.shortcuts import render
from coronawebsite.models import Posizione
from coronawebsite.models import displaydata
from django.db import connection


def previsioni_liste(request):
    return render(request,'sezioneprevisioni/previsioni_liste.html')


def mostraValori(request):
    #return HttpResponse('homepage')
    #resultsdisplay = Posizione.objects.all()
    #return render(request,'homepage.html',{'Posizione':resultsdisplay})#homepage.html è dentro la cartella
    nomeStato = request.GET.get('nomeStato', '')
    nomeRegione = request.GET.get('nomeRegione', '')

    if nomeStato == '' and nomeRegione == '':
        cursor=connection.cursor()
        cursor.execute("select SUM(VALORICOVIDSTATO.nuoviPositivi), SUM(VALORICOVIDSTATO.nuoviMorti), SUM(VALORICOVIDSTATO.nuoviGuariti), VALORICOVIDSTATO.dataValoriCovid from VALORICOVIDSTATO GROUP BY VALORICOVIDSTATO.dataValoriCovid ")
        resultsNuovi=cursor.fetchall()
        cursor.execute("select SUM(VALORICOVIDSTATO.positiviTotali), SUM(VALORICOVIDSTATO.mortiTotali), SUM(VALORICOVIDSTATO.guaritiTotali), VALORICOVIDSTATO.dataValoriCovid from VALORICOVIDSTATO GROUP BY VALORICOVIDSTATO.dataValoriCovid ")
        resultsTotali=cursor.fetchall()
        cursor.execute("select * from STATO")
        results2=cursor.fetchall()
        return render(request, 'sezioneprevisioni/previsioni_liste.html',{'datiDeiNuovi':resultsNuovi,'datiDeiTotali':resultsTotali,'stati':results2,'nienteRegioni':'ok','statoCliccato':'no'})

    elif nomeStato != '' and nomeRegione == '':
        nomeStato = nomeStato.replace('_', ' ')
        cursor=connection.cursor()
        cursor.execute("select VALORICOVIDSTATO.nuoviPositivi, VALORICOVIDSTATO.nuoviMorti, VALORICOVIDSTATO.nuoviGuariti, VALORICOVIDSTATO.dataValoriCovid from VALORICOVIDSTATO where nomeStato='"+nomeStato+"'")
        resultsNuovi=cursor.fetchall()
        cursor.execute("select VALORICOVIDSTATO.positiviTotali, VALORICOVIDSTATO.mortiTotali, VALORICOVIDSTATO.guaritiTotali, VALORICOVIDSTATO.dataValoriCovid from VALORICOVIDSTATO where nomeStato='"+nomeStato+"'")
        resultsTotali=cursor.fetchall()
        cursor.execute("select nomeRegione from REGIONE where nomeStato='"+nomeStato+"'")
        results2=cursor.fetchall()
        if len(results2) != 0:
            return render(request, 'sezioneprevisioni/previsioni_liste.html',{'datiDeiNuovi':resultsNuovi,'datiDeiTotali':resultsTotali,'regioni':results2, 'nienteRegioni': 'no','statoCliccato':'ok'})
        else:
            return render(request, 'sezioneprevisioni/previsioni_liste.html',{'datiDeiNuovi':resultsNuovi,'datiDeiTotali':resultsTotali,'regioni':results2, 'nienteRegioni': 'no','statoCliccato':'no'})

    elif nomeStato != '' and nomeRegione != '':
        nomeStato = nomeStato.replace('_', ' ')
        nomeRegione = nomeRegione.replace('_', ' ')
        cursor=connection.cursor()
        cursor.execute("select VALORICOVIDREGIONE.nuoviPositivi, VALORICOVIDREGIONE.nuoviMorti, VALORICOVIDREGIONE.nuoviGuariti, VALORICOVIDREGIONE.dataValoriCovid from VALORICOVIDREGIONE where nomeStato='"+nomeStato+"' AND nomeRegione='"+nomeRegione+"'")
        resultsNuovi=cursor.fetchall()
        cursor.execute("select VALORICOVIDREGIONE.positiviTotali, VALORICOVIDREGIONE.mortiTotali, VALORICOVIDREGIONE.guaritiTotali, VALORICOVIDREGIONE.dataValoriCovid from VALORICOVIDREGIONE where nomeStato='"+nomeStato+"' AND nomeRegione='"+nomeRegione+"'")
        resultsTotali=cursor.fetchall()
        return render(request, 'sezioneprevisioni/previsioni_liste.html',{'datiDeiNuovi':resultsNuovi,'datiDeiTotali':resultsTotali, 'nienteRegioni': 'no','statoCliccato':'no'})
      

        