from django.apps import AppConfig


class SezioneprevisioniConfig(AppConfig):
    name = 'sezioneprevisioni'
