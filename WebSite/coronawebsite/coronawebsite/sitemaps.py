from django.contrib.sitemaps import Sitemap
from django.shortcuts import reverse
from .models import Snippet
#from .models import Snippet
from django.contrib import sitemaps
from django.urls import reverse

'''
class StaticViewSitemap(Sitemap):
    changefreq = 'huorly'

    def items(self):
        return ['whoweare']


    def location(self,item):
        return reverse(item)



class SnippetSitemap(Sitemap):

    def items(self):
        return Snippet.objects.all()

'''


class StaticViewSitemap(sitemaps.Sitemap):
    priority = 0.5
    changefreq = 'daily'

    def items(self):
        return ['homepage', 'predictPage', 'whoweare']

    def location(self, item):
        return reverse(item)
class SnippetSitemap(Sitemap):
    def items(self):
        return Snippet.objects.all()





