from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404
from coronawebsite.models import Posizione
from coronawebsite.models import displaydata
from django.db import connection
from django.http import HttpResponseRedirect
from .forms import NameForm
import math
#from .models import Snippet

from django.shortcuts import get_object_or_404
from .models import Snippet


def homepage(request):
    #return HttpResponse('homepage')
    #resultsdisplay = Posizione.objects.all()
    #return render(request,'homepage.html',{'Posizione':resultsdisplay})#homepage.html è dentro la cartella
    nomeStato = request.GET.get('nomeStato', '')
    nomeRegione = request.GET.get('nomeRegione', '')

    if nomeStato == '' and nomeRegione == '':
        cursor=connection.cursor()
        cursor.execute("select SUM(VALORICOVIDSTATO.nuoviPositivi), SUM(VALORICOVIDSTATO.nuoviMorti), SUM(VALORICOVIDSTATO.nuoviGuariti), VALORICOVIDSTATO.dataValoriCovid from VALORICOVIDSTATO GROUP BY VALORICOVIDSTATO.dataValoriCovid ")
        resultsNuovi=cursor.fetchall()
        cursor.execute("select SUM(VALORICOVIDSTATO.positiviTotali), SUM(VALORICOVIDSTATO.mortiTotali), SUM(VALORICOVIDSTATO.guaritiTotali), VALORICOVIDSTATO.dataValoriCovid from VALORICOVIDSTATO GROUP BY VALORICOVIDSTATO.dataValoriCovid ")
        resultsTotali=cursor.fetchall()
        cursor.execute("select * from STATO")
        results2=cursor.fetchall()
        return render(request, 'homepage.html',{'datiDeiNuovi':resultsNuovi,'datiDeiTotali':resultsTotali,'stati':results2,'nienteRegioni':'ok','statoCliccato':'no'})

    elif nomeStato != '' and nomeRegione == '':
        nomeStato = nomeStato.replace('_', ' ')
        cursor=connection.cursor()
        cursor.execute("select VALORICOVIDSTATO.nuoviPositivi, VALORICOVIDSTATO.nuoviMorti, VALORICOVIDSTATO.nuoviGuariti, VALORICOVIDSTATO.dataValoriCovid from VALORICOVIDSTATO where nomeStato='"+nomeStato+"'")
        resultsNuovi=cursor.fetchall()
        cursor.execute("select VALORICOVIDSTATO.positiviTotali, VALORICOVIDSTATO.mortiTotali, VALORICOVIDSTATO.guaritiTotali, VALORICOVIDSTATO.dataValoriCovid from VALORICOVIDSTATO where nomeStato='"+nomeStato+"'")
        resultsTotali=cursor.fetchall()
        cursor.execute("select nomeRegione from REGIONE where nomeStato='"+nomeStato+"'")
        results2=cursor.fetchall()
        if len(results2) != 0:
            return render(request, 'homepage.html',{'datiDeiNuovi':resultsNuovi,'datiDeiTotali':resultsTotali,'regioni':results2, 'nienteRegioni': 'no','statoCliccato':'ok'})
        else:
            return render(request, 'homepage.html',{'datiDeiNuovi':resultsNuovi,'datiDeiTotali':resultsTotali,'regioni':results2, 'nienteRegioni': 'no','statoCliccato':'no'})

    elif nomeStato != '' and nomeRegione != '':
        nomeStato = nomeStato.replace('_', ' ')
        nomeRegione = nomeRegione.replace('_', ' ')
        cursor=connection.cursor()
        cursor.execute("select VALORICOVIDREGIONE.nuoviPositivi, VALORICOVIDREGIONE.nuoviMorti, VALORICOVIDREGIONE.nuoviGuariti, VALORICOVIDREGIONE.dataValoriCovid from VALORICOVIDREGIONE where nomeStato='"+nomeStato+"' AND nomeRegione='"+nomeRegione+"'")
        resultsNuovi=cursor.fetchall()
        cursor.execute("select VALORICOVIDREGIONE.positiviTotali, VALORICOVIDREGIONE.mortiTotali, VALORICOVIDREGIONE.guaritiTotali, VALORICOVIDREGIONE.dataValoriCovid from VALORICOVIDREGIONE where nomeStato='"+nomeStato+"' AND nomeRegione='"+nomeRegione+"'")
        resultsTotali=cursor.fetchall()
        return render(request, 'homepage.html',{'datiDeiNuovi':resultsNuovi,'datiDeiTotali':resultsTotali, 'nienteRegioni': 'no','statoCliccato':'no'})




def about(request):
    #return HttpResponse('about')
    return render(request,'about.html')

def whoweare(request):
    #return HttpResponse('about')
    return render(request,'whoweare.html')


def predictPage(request):
    #return HttpResponse('about')
    #return HttpResponse('homepage')
    #resultsdisplay = Posizione.objects.all()
    #return render(request,'homepage.html',{'Posizione':resultsdisplay})#homepage.html è dentro la cartella
    nomeStato = request.GET.get('nomeStato', '')
    nomeRegione = request.GET.get('nomeRegione', '')

    mobility = request.GET.get('mobility', '')
    print("mobility: ", mobility)

    ######################################################
    ######################################################
    ############ SEZIONE DELLA PREVISIONE#################
    ######################################################
    ######################################################

    if nomeStato == '' and nomeRegione == '':
        cursor=connection.cursor()
        cursor.execute("select SUM(VALORICOVIDSTATO.nuoviPositivi), SUM(VALORICOVIDSTATO.nuoviMorti), SUM(VALORICOVIDSTATO.nuoviGuariti), VALORICOVIDSTATO.dataValoriCovid from VALORICOVIDSTATO GROUP BY VALORICOVIDSTATO.dataValoriCovid ")
        resultsNuovi=cursor.fetchall()
        cursor.execute("select SUM(VALORICOVIDSTATO.positiviTotali), SUM(VALORICOVIDSTATO.mortiTotali), SUM(VALORICOVIDSTATO.guaritiTotali), VALORICOVIDSTATO.dataValoriCovid from VALORICOVIDSTATO GROUP BY VALORICOVIDSTATO.dataValoriCovid ")
        resultsTotali=cursor.fetchall()
        cursor.execute("select * from STATO")
        results2=cursor.fetchall()
        return render(request, 'predictPage.html',{'datiDeiNuovi':resultsNuovi,'datiDeiTotali':resultsTotali,'stati':results2,'nienteRegioni':'ok','statoCliccato':'no','percentageDeaths':0.01})

    elif nomeStato != '' and nomeRegione == '':
        nomeStato = nomeStato.replace('_', ' ')
        cursor=connection.cursor()
        cursor.execute("SELECT movingAvgPos FROM VALORICOVIDSTATO WHERE nomeStato='"+nomeStato+"'")
        resultsMoveAvgPos = cursor.fetchall()
        vettore_MediaMobile_NuoviPositivi = resultsMoveAvgPos
        cursor.execute("SELECT movingAvgGua FROM VALORICOVIDSTATO WHERE nomeStato='"+nomeStato+"'")
        resultsMoveAvgGua = cursor.fetchall()
        vettore_MediaMobile_NuoviGuariti = resultsMoveAvgGua
        cursor.execute("SELECT movingAvgMorti FROM VALORICOVIDSTATO WHERE nomeStato='"+nomeStato+"'")
        resultsMoveAvgMorti = cursor.fetchall()
        vettore_MediaMobile_NuoviMorti = resultsMoveAvgMorti
        cursor=connection.cursor()
        cursor.execute("select VALORICOVIDSTATO.nuoviPositivi, VALORICOVIDSTATO.nuoviMorti, VALORICOVIDSTATO.nuoviGuariti, VALORICOVIDSTATO.dataValoriCovid from VALORICOVIDSTATO where nomeStato='"+nomeStato+"'")
        resultsNuovi=cursor.fetchall()
        cursor.execute("select VALORICOVIDSTATO.positiviTotali, VALORICOVIDSTATO.mortiTotali, VALORICOVIDSTATO.guaritiTotali, VALORICOVIDSTATO.dataValoriCovid from VALORICOVIDSTATO where nomeStato='"+nomeStato+"'")
        resultsTotali=cursor.fetchall()
        cursor.execute("select nomeRegione from REGIONE where nomeStato='"+nomeStato+"'")
        results2=cursor.fetchall()

        percentageDeaths=percentageDeathsCalc(nomeStato)
        #return
        if len(results2) != 0:
            return render(request, 'predictPage.html',{'datiDeiNuovi':resultsNuovi,'datiDeiTotali':resultsTotali,'regioni':results2, 'nienteRegioni': 'no','statoCliccato':'ok','mediaMobileNuoviPositivi':vettore_MediaMobile_NuoviPositivi,'mediaMobileNuoviGuariti':vettore_MediaMobile_NuoviGuariti,'mediaMobileNuoviMorti':vettore_MediaMobile_NuoviMorti,'percentageDeaths':percentageDeaths})
        else:
            return render(request, 'predictPage.html',{'datiDeiNuovi':resultsNuovi,'datiDeiTotali':resultsTotali,'regioni':results2, 'nienteRegioni': 'ok','statoCliccato':'ok','mediaMobileNuoviPositivi':vettore_MediaMobile_NuoviPositivi,'mediaMobileNuoviGuariti':vettore_MediaMobile_NuoviGuariti,'mediaMobileNuoviMorti':vettore_MediaMobile_NuoviMorti,'percentageDeaths':percentageDeaths})

    elif nomeStato != '' and nomeRegione != '':
        nomeStato = nomeStato.replace('_', ' ')
        nomeRegione = nomeRegione.replace('_', ' ')
        cursor=connection.cursor()
        cursor.execute("SELECT movingAvgPos FROM VALORICOVIDREGIONE WHERE nomeStato='"+nomeStato+"' AND nomeRegione='"+nomeRegione+"'")
        resultsMoveAvgPos = cursor.fetchall()
        vettore_MediaMobile_NuoviPositivi = resultsMoveAvgPos
        cursor.execute("SELECT movingAvgGua FROM VALORICOVIDREGIONE WHERE nomeStato='"+nomeStato+"' AND nomeRegione='"+nomeRegione+"'")
        resultsMoveAvgGua = cursor.fetchall()
        vettore_MediaMobile_NuoviGuariti = resultsMoveAvgGua
        cursor.execute("SELECT movingAvgMorti FROM VALORICOVIDREGIONE WHERE nomeStato='"+nomeStato+"' AND nomeRegione='"+nomeRegione+"'")
        resultsMoveAvgMorti = cursor.fetchall()
        vettore_MediaMobile_NuoviMorti = resultsMoveAvgMorti
        cursor=connection.cursor()
        cursor.execute("select VALORICOVIDREGIONE.nuoviPositivi, VALORICOVIDREGIONE.nuoviMorti, VALORICOVIDREGIONE.nuoviGuariti, VALORICOVIDREGIONE.dataValoriCovid from VALORICOVIDREGIONE where nomeStato='"+nomeStato+"' AND nomeRegione='"+nomeRegione+"'")
        resultsNuovi=cursor.fetchall()
        cursor.execute("select VALORICOVIDREGIONE.positiviTotali, VALORICOVIDREGIONE.mortiTotali, VALORICOVIDREGIONE.guaritiTotali, VALORICOVIDREGIONE.dataValoriCovid from VALORICOVIDREGIONE where nomeStato='"+nomeStato+"' AND nomeRegione='"+nomeRegione+"'")
        resultsTotali=cursor.fetchall()
        percentageDeaths=percentageDeathsCalc(nomeStato)
        return render(request, 'predictPage.html',{'datiDeiNuovi':resultsNuovi,'datiDeiTotali':resultsTotali, 'nienteRegioni': 'ok','statoCliccato':'ok','mediaMobileNuoviPositivi':vettore_MediaMobile_NuoviPositivi,'mediaMobileNuoviGuariti':vettore_MediaMobile_NuoviGuariti,'mediaMobileNuoviMorti':vettore_MediaMobile_NuoviMorti,'percentageDeaths':percentageDeaths})

#Calcolo percentuali dei morti per stato
def percentageDeathsCalc(nomeStato):
    cursor=connection.cursor()
    cursor.execute("select positiviTotali,mortiTotali from VALORICOVIDSTATO where nomeStato='"+nomeStato+"' order by dataValoriCovid limit 1")
    posMortFirstDate=cursor.fetchall()
    cursor.execute("select positiviTotali,mortiTotali from VALORICOVIDSTATO where nomeStato='"+nomeStato+"' order by dataValoriCovid desc limit 1")
    posMortLastDate=cursor.fetchall()
    posTotFirstDate=0
    posTotLastDate=0
    morTotFirstDate=0
    morTotLastDate=0
    for row in posMortFirstDate:
        posTotFirstDate=row[0]
        morTotFirstDate=row[1]
    for row in posMortLastDate:
        posTotLastDate=row[0]
        morTotLastDate=row[1]
    diffPos=posTotLastDate-posTotFirstDate
    diffMorti=morTotLastDate-morTotFirstDate
    percentageDeaths=round(diffMorti/diffPos,4)
    return percentageDeaths




'''

def snippet_detail(request, slug):
    snippet = get_object_or_404(Snippet, slug=slug)
    return HttpResponse(f'Detail view for the slug of {slug}')

    '''



def snippet_detail(request, slug):
    snippet = get_object_or_404(Snippet, slug=slug)
    return HttpResponse(f'the detailview for slug of {slug}')