from django.contrib import admin
from django.urls import path
from django.conf.urls import url,include
from . import views #il punto indica la stessa directory
from django.contrib.sitemaps.views import sitemap
from coronawebsite.sitemaps import StaticViewSitemap#, SnippetSitemap
from coronawebsite.views import snippet_detail
from django.views.generic.base import TemplateView


sitemaps = {
    'static': StaticViewSitemap
}



urlpatterns = [
    path('admin/', admin.site.urls),
    path('sitemap.xml', sitemap, {'sitemaps': sitemaps}),
    #path('<slug:slug>/', views.snippet_detail),
    url(r'^sezioneprevisioni/',include('sezioneprevisioni.urls')),
    url(r'^about/$', views.about),
    url(r'^predictPage/$', views.predictPage, name='predictPage'),
    url(r'^whoweare/$', views.whoweare, name='whoweare'),
    #url(r'^$', views.homepage),
    path('',views.homepage, name='homepage'),
    #path(calcolaPrevisione, views.yourSort),
    #path('',views.tablesjoin)
    path('<slug:slug>/', snippet_detail),
    path(
        "robots.txt",
        TemplateView.as_view(template_name="robots.txt", content_type="text/plain"),
    ),

]
